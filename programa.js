class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.602387, -74.096285];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono(posicion, configuracion){

        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);

    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.602387, -74.096285]);
miMapa.colocarCirculo([4.602387, -74.096285], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 100
});
miMapa.colocarPoligono([[4.602387, -74.096285],[4.599729, -74.101448],[4.595684, -74.096211]], {
    color: 'blue',
    fillColor: '#f34',
    fillOpacity: 0.5,
});


